package main

import (
	"fmt"
	"go-programming/testing/03-examples/01/mate"
)

func main() {

	fmt.Println(mate.Sum(2, 4, 5))
	fmt.Println(mate.Sum(5, 5, 0))

}
