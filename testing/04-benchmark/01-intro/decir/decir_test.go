package decir

import (
	"fmt"
	"testing"
)

func TestSaludar(t *testing.T) {
	s := Saludar("Alejandro")
	if s != "Bienvenido querido Alejandro" {
		t.Error("Excpected", "Bienvenido querido Alejandro", "Got", s)
	}
}

func ExampleSaludar() {
	fmt.Println(Saludar("Alejandro"))
	//Output:
	//Bienvenido querido Alejandro
}

func Benchmark(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Saludar("Alejandro")

	}

}
