package main

import "fmt"

func main() {
	fmt.Println("La suma es 2 + 4 es:", miSuma(2, 4))
	fmt.Println("La suma es 2 + 4 es:", miSuma(1, 3))
	fmt.Println("La suma es 2 + 4 es:", miSuma(4, 6))
	fmt.Println("La suma es 2 + 4 es:", miSuma(3, 8))

}

func miSuma(xi ...int) int {
	var sum int
	for _, v := range xi {
		sum += v

	}
	return sum
}
