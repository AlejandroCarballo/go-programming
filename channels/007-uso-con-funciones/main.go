package main

import "fmt"

func main() {

	c := make(chan int)

	//enviar
	go enviar(c)

	recibir(c)

	fmt.Println("Finalizando.")

}

//canal send only como parametros de un una función
func enviar(c chan<- int) {
	c <- 42

}

func recibir(c chan int) {
	fmt.Println(<-c)
}
